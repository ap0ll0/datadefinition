#!/usr/bin/env python3
"""Craft kcidb data."""

from kcidb_io.schema import is_valid
from kcidb_io.schema.v3 import JSON
from kcidb_io.schema.v3 import JSON_BUILD
from kcidb_io.schema.v3 import JSON_REVISION
from kcidb_io.schema.v3 import JSON_TEST
from kcidb_io.schema.v3 import JSON_VERSION_MAJOR
from kcidb_io.schema.v3 import JSON_VERSION_MINOR

from rcdefinition.rc_data import DefinitionBase
from rcdefinition.rc_data import _NO_DEFAULT


def craft_kcidb_data(revisions, builds, tests):
    """Craft kcidb schema from internal data."""
    craft = {"version": dict(major=JSON_VERSION_MAJOR,
                             minor=JSON_VERSION_MINOR),
             "revisions": revisions, "builds": builds, "tests": tests}
    return craft


class KCIDBObject(DefinitionBase):
    """KCIDB v3 schema (de)serialization."""

    # dict object that describes metadata
    meta = JSON
    # type conversion table
    _conv_table = {'object': dict,
                   'string': str,
                   'number': int,
                   'array': list,
                   'boolean': bool}

    def __init__(self, dict_data=None):
        """Create an object."""
        self.from_meta(self.meta)
        super().__init__(dict_data)

    def from_meta(self, schema):
        """Modify annotations to contain keys/types according to metadata."""
        self.__annotations__ = {}
        properties = schema['properties']
        required = schema['required']

        for key, value in properties.items():
            actual_type = KCIDBObject._conv_table[value['type']]
            self.__annotations__[key] = actual_type
            if key in required:
                setattr(self, key, _NO_DEFAULT)


class Revision(KCIDBObject):
    """KCIDB v3 Revision object."""

    meta = JSON_REVISION

    def is_valid(self):
        """Check if the data in this object is valid."""
        return is_valid(craft_kcidb_data([self.to_mapping()], [], []))


class Build(KCIDBObject):
    """KCIDB v3 Build object."""

    meta = JSON_BUILD

    def is_valid(self):
        """Check if the data in this object is valid."""
        return is_valid(craft_kcidb_data([], [self.to_mapping()], []))


class Test(KCIDBObject):
    """KCIDB v3 Test object."""

    meta = JSON_TEST

    def is_valid(self):
        """Check if the data in this object is valid."""
        return is_valid(craft_kcidb_data([], [], [self.to_mapping()]))
