# Copyright (c) 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Gitlab api interaction tests."""
from abc import ABCMeta
from enum import Enum
import io
from typing import List
import unittest
from unittest import mock

# The unused import of const below is intentional; we just check that the import doesn't break
# anything.
# pylint: disable=unused-import
from rcdefinition import const  # noqa: F401
from rcdefinition.rc_data import DefinitionBase
from rcdefinition.rc_data import RunnerData
from rcdefinition.rc_data import SKTData
from rcdefinition.rc_data import _NO_DEFAULT
from rcdefinition.rc_data import parse_config_data


class Enum2Test(Enum):
    """A class to test enum serialization."""
    A = 1
    B = 2
    C = 3


class TestTriggerVariables(unittest.TestCase):
    """Test TriggerVariables class."""

    def test_parse_config_data(self):
        """Ensure parse_config_data works."""
        data = "key=value"

        result1 = parse_config_data(data, False)
        self.assertEqual(result1['key'], 'value')

        result2 = parse_config_data('[data]\n' + data, True)

        self.assertEqual(result2['data']['key'], 'value')

        result3 = parse_config_data('[data]\na=b\n[data2]\nkey=value')
        self.assertEqual(result3['data2']['key'], 'value')

    def test_parse_config_data_empty(self):
        """Ensure parse_config_data returns {} for empty."""
        result = parse_config_data('')
        self.assertEqual(result, {})


class FakeInnerClass(DefinitionBase):
    """An inner fake testing class with 1 annotated attribute."""
    key2: str = None


class FakeClass(DefinitionBase):
    """A fake testing class with 1 annotated attribute."""
    key: str = _NO_DEFAULT
    nonekey: str = None
    inner: FakeInnerClass = None
    lists: List[FakeInnerClass] = None
    en: Enum2Test = Enum2Test(1)


class BadClass(DefinitionBase):
    """A testing class that is supposed to fail serialization."""
    somekey: ABCMeta


class TestDefinitionBase(unittest.TestCase):
    """Test using DefinitionBase to create new classes."""

    def setUp(self) -> None:
        self.fake = FakeClass({'key': 'value', 'nonekey': None, 'inner': {},
                               'lists': [{}, {}]})

    def test_init(self):
        """Ensure __init__() works."""
        self.assertEqual(self.fake.key, 'value')

    def test_items(self):
        # pylint: disable=W1116
        """Ensure .items() works."""
        # check that simple attribute is in .items()
        self.assertIn(('key', 'value'), self.fake.items())

        # check that complex attribute is in .items() and is converted by
        # its annotation
        self.assertTrue(isinstance(self.fake.inner, FakeInnerClass))
        self.assertTrue(isinstance(self.fake.lists, List))

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_dynamic_keys(self, mock_stderr):
        """Ensure that keys without annotations are created as well."""
        fake = FakeClass({'key': None, 'blahNoAnnotation': 'test'})
        self.assertIn(('blahNoAnnotation', 'test'), fake.items())

        mock_stderr.seek(0)
        data = mock_stderr.read()
        self.assertIn('blahNoAnnotation', data)

    def test_required_keys(self):
        """Ensure that keys with _NO_DEFAULT are required."""
        with self.assertRaises(TypeError):
            FakeClass({})

    @mock.patch('sys.stderr.write')
    def test_missing_keys(self, mock_write):
        # pylint: disable=no-self-use
        """Ensure missing keys are caught."""
        _ = FakeClass({'key': 'value', 'h': 0})
        mock_write.assert_called_with('code-issue: following keys are not part of the '
                                      'datastructure (FakeClass) definition: h\n')

    @mock.patch('sys.stderr.write')
    def test_invalid_serialization(self, mock_write):
        """Ensure error is indicated when invalid serialization is attempted."""
        with self.assertRaises(TypeError):
            _ = BadClass()
            mock_write.assert_called_with("conversion failed for somekey {} <class 'abc.ABCMeta'>")

    def test_serialize_deserialize_enum(self):
        """Ensure enum serialization works back and forth."""
        fake = FakeClass({'key': 'value', 'en': Enum2Test(1)})
        data = fake.to_mapping()
        deserialized = FakeClass(data)

        self.assertEqual(deserialized.key, 'value')
        self.assertEqual(deserialized.en, Enum2Test(1))


class TestSKTData(unittest.TestCase):
    """Test SKTData."""

    def test_serialize_deserialize(self):
        """Test that de(serialize) works and that 2 sections are present."""
        skt_data = SKTData({'state': {'retcode': 1},
                            'runner': {'excluded_hostnames': 'content'}})
        data = skt_data.serialize()
        self.assertIn('[state]', data)
        self.assertIn('[runner]', data)

        obj = SKTData.deserialize(data)
        self.assertEqual(obj.state.retcode, 1)
        self.assertEqual(obj.runner.excluded_hostnames, 'content')

    def test_serialize(self):
        """Test that de(serialize) works when optional section is missing."""
        skt_data = SKTData({'state': {'retcode': 1}})
        data = skt_data.serialize()
        self.assertIn('[state]', data)

        obj = SKTData.deserialize(data)
        self.assertEqual(obj.state.retcode, 1)

        skt_data = SKTData({'runner': {'excluded_hostnames': 'content'}})
        data = skt_data.serialize()
        self.assertIn('[runner]', data)

        obj = SKTData.deserialize(data)
        self.assertEqual(obj.runner.excluded_hostnames, 'content')

    @mock.patch('sys.stderr', new_callable=io.StringIO)
    def test_deserialize(self, mock_stderr):
        # pylint: disable=E1101,W0613
        """Ensure Runner section is created."""
        data = '''[fake]
        key = value
        [fake2]
        key2 = value2
        '''

        skt_data = SKTData.deserialize(data)

        self.assertTrue(isinstance(skt_data.runner, RunnerData))

        # test that unknown keys are annotated as strings
        self.assertEqual(skt_data.fake, "{'key': 'value'}")
        self.assertEqual(skt_data.fake2, "{'key2': 'value2'}")

        result = skt_data.to_mapping()
        self.assertEqual(result, {'fake': "{'key': 'value'}", 'fake2': "{'key2': 'value2'}",
                                  'state': {'patch_data': [], 'patch_subjects': []}, 'runner': {},
                                  'revision': {}, 'build': {'valid': True, 'misc': {}}})
