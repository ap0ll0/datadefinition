# Copyright (c) 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Gitlab api interaction tests."""
import unittest

from kcidb_wrap import v3


class TestKCIDBWrap(unittest.TestCase):
    """Test KCIDBObject class."""

    def test_craft_build(self):
        """Ensure crafting and serialization works for build object."""
        raw_data = {
            'revision_id': 'decd6167bf4f6bec1284006d0522381b44660df3',
            'id': 'redhat:1',
            'origin': 'redhat',
            'description': 'data data',
            'start_time': '2019-11-14T00:07:00Z',
            'duration': 321,
            'architecture': 'x86_64',
            'command': 'make bzImage',
            'compiler': 'foo 1.2',
            'input_files': [],
            'output_files': [
                {'name': 'kernel.tar.gz',
                 'url': 'http://s3.server/kernel.tar.gz'}],
            'config_name': 'fedora',
            'config_url': 'http://s3.server/kernel.config',
            'log_url': 'http://s3.server/testfile.log',

            'misc': {'job_id': 3, 'pipeline_id': 3},
            'valid': True,
        }

        build = v3.Build(raw_data)
        self.assertTrue(build.is_valid())

    def test_craft_revision(self):
        """Ensure crafting and serialization works for revision object."""
        raw_data = {"id": "ab1164e86e1898e3df162a48d7ab170aa396872f",
                    "origin": "redhat",
                    "tree_name": "rhel", "git_repository_url": "https://repo",
                    "git_commit_hash": "deadbeefdeadbeefdeadbeefdeadbeefdeadbeef",
                    "git_commit_name": "kernel-4.18.0-0.el7",
                    "git_repository_branch": "rhel",
                    "description": "[patch] [patch]",
                    "publishing_time": "2000-12-02T10:00:00.000000Z", "discovery_time": "2000-12-02T10:00:00.000000Z",
                    "contacts": [], "valid": True, "misc": {"pipeline_id": "1234"}}

        revision = v3.Revision(raw_data)
        self.assertTrue(revision.is_valid())

    def test_craft_test(self):
        """Ensure crafting and serialization works for test object."""
        raw_data = {"build_id": "redhat:1234", "id": "redhat:3",
                    "environment": {"description": "hostname"},
                    "description": "Boot test", "waived": False, "start_time": "2000-12-02T10:00:00.000000Z",
                    "duration": 343, "misc": {"debug": False, "targeted": False,
                                              "fetch_url": "https://fetch",
                                              "beaker": {"task_id": 3, "recipe_id": 123456,
                                                         "finish_time": "2000-12-02T10:00:00.000000Z"},
                                              "job": {"id": "1036855", "name": "test x86_64", "stage": "test",
                                                      "started_at": "2000-12-02T10:00:00.000000Z",
                                                      "created_at": "2000-12-02T10:00:00.000000Z", "finished_at": None,
                                                      "duration": 123, "test_hash": None, "tag": None,
                                                      "commit_message_title": None, "kernel_version": None},
                                              "pipeline": {"id": "618657", "variables": {},
                                                           "started_at": "2000-12-02T10:00:00.000000Z",
                                                           "created_at": "2000-12-02T10:00:00.000000Z",
                                                           "finished_at": "2000-12-02T10:00:00.000000Z",
                                                           "duration": 7158,
                                                           "ref": "rhel-c6cbbcb6-1a70-4509-90e2-2949d9b284a3",
                                                           "sha": "deadbeefdeadbeefdeadbeefdeadbeefdeadbeef",
                                                           "project": {"id": 2,
                                                                       "path_with_namespace":
                                                                           "cki-project/cki-pipeline"}}},
                    "status": "PASS", "path": "boot", "origin": "redhat"}

        test = v3.Test(raw_data)
        self.assertTrue(test.is_valid())
