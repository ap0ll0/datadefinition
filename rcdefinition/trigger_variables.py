#!/usr/bin/env python3
# Copyright (c) 2018 - 2020 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Pipeline trigger variables definition."""
import os

from rcdefinition.rc_data import DefinitionBase


class TriggerVariables(DefinitionBase):
    # pylint: disable=R0902
    """Serialization class for trigger variables."""

    # Projects should always pass data by passing this object, or
    # representation created by this object's serialize/deserialize
    # methods. This ensures all necessary data is well-defined and present
    # during __init__. If something is missing and is required, __init__
    # must fail.

    # CI/Testing variables

    # https://gitlab.com/cki-project/cki-lib/-/archive/SHA/cki-lib-SHA.tar.gz
    # an url in this format will force the CI to install a different cki-lib
    # in prepare stage
    cki_lib_targz_url: str = None
    debug_email: str = None  # affects email / reporting, forces single address
    skip_beaker: str = None

    # All-purpose email & reporting variables
    mail_add_maintainers_to: str = None
    mail_to_task_owner: str = None
    mail_bcc: str = None
    mail_cc: str = None
    mail_from: str = None
    mail_to: str = None
    manual_review_mail_to: str = None
    report_template: str = None
    report_types: str = None  # to be removed: legacy pipelines only
    require_manual_review: str = None
    subject: str = None  # used for stable queue trigger as well
    send_report_to_upstream: str = None
    send_report_on_success: str = None

    # General CI variables
    cki_project: str = None
    cki_pipeline_id: str = None
    cki_pipeline_branch: str = None
    cki_pipeline_type: str = None
    name: str = None  # trigger name
    original_pipeline: str = None  # used for UMB messaging
    original_pipeline_id: int = None  # used for UMB messaging
    result_pipe: str = None  # used for UMB messaging
    retrigger: str = None

    # UMB messaging CI variables
    send_ready_for_test_pre: str = None
    send_ready_for_test_post: str = None

    # Gitlab CI variables
    pipeline_definition_branch_override: str = None
    pipeline_definition_repository_override: str = None
    status: str = None

    # Kernel artifacts variables: patch and patchwork related variables
    cover_letter: str = None
    date: str = None  # to be removed: legacy pipelines only
    event_id: int = None
    last_patch_id: str = None
    message_id: str = None
    patch_urls: str = None  # used for stable queue trigger as well
    patchwork_url: str = None
    patchwork_project: str = None
    submitter: str = None
    skipped_series: str = None

    #  Kernel artifacts variables: general-purpose variables
    commit_hash: str = None
    title: str = None

    # Kernel artifacts variables: Brew trigger variables
    data: str = None
    owner: str = None
    package_name: str = None
    rpm_release: str = None
    team_email: str = None
    team_name: str = None

    # Kernel artifacts variables: baseline trigger/stable queue variables
    branch: str = None
    git_url: str = None
    make_target: str = None

    # Kernel artifacts variables: stable queue
    queue_commit_hash: str = None
    queue_url: str = None

    # Most likely legacy variables
    merge_branch: str = None
    merge_tree: str = None

    def pipeline_vars_from_env(self):
        # pylint: disable=no-member
        """Return all trigger variables as dict."""
        for attr in self.__annotations__:
            value = os.environ.get(attr, None)
            if value is not None:
                setattr(self, attr, value)

        return self.to_mapping()

    def print_var_names(self):
        # pylint: disable=no-member
        """Print names of trigger variables."""
        print(' '.join(self.__annotations__))
